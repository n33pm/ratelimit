package ratelimit

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"time"
)

func TestNewBucketRefillAndLimitNull(t *testing.T) {

	b, err := NewBucket(time.Second*1, 0, 0)

	assert.Nil(t, b)
	assert.Error(t, err, "refill AND limit => 0")
}

func TestNewBucketRefillNull(t *testing.T) {

	duration := time.Second * 1
	refill := uint(0)
	limit := uint(10)

	b, err := NewBucket(duration, refill, limit)

	time.Sleep(duration * 2)

	assert.Equal(t, b.refillTime, duration)
	assert.Equal(t, b.limit, refill)
	assert.Equal(t, b.refillAmount, limit)
	assert.Nil(t, err)
}

func TestNewBucket(t *testing.T) {

	duration := time.Second * 1
	refill := uint(20)
	limit := uint(10)

	b, err := NewBucket(duration, refill, limit)

	time.Sleep(duration * 2)

	assert.Equal(t, b.refillTime, duration)
	assert.Equal(t, b.refillAmount, refill)
	assert.Equal(t, b.limit, limit)
	assert.Nil(t, err)
}
