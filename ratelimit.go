package ratelimit

import (
	"time"
	"errors"
)

type Bucket struct {
	refillTime   time.Duration
	refillAmount uint
	limit        uint
	Slot         chan bool
	ticker       *time.Ticker
}

func NewBucket(refillTime time.Duration, refillAmount uint, limit uint) (*Bucket, error) {

	if refillAmount == 0 && limit == 0 {
		return nil, errors.New("refill AND limit => 0")
	}

	if refillAmount == 0 && limit > 0 {
		limit, refillAmount = refillAmount, limit
	}

	b := &Bucket{
		refillTime:   refillTime,
		refillAmount: refillAmount,
		limit:        limit,
		Slot:         make(chan bool, limit),
	}

	b.ticker = time.NewTicker(refillTime)

	go func(b *Bucket) {
		for range b.ticker.C {
			go b.refill()
		}
	}(b)

	return b, nil
}

func (b *Bucket) refill() {

	amount := b.refillAmount
	if uint(len(b.Slot))+b.refillAmount > b.limit && b.limit != 0 {
		amount = b.limit - uint(len(b.Slot))
	}

	for i := uint(0); i < amount; i++ {
		b.Slot <- true
	}
}
