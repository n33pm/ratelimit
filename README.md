# _wugy/ratelimit

[![pipeline status](https://gitlab.com/_wugy/ratelimit/badges/master/pipeline.svg)](https://gitlab.com/_wugy/ratelimit/commits/master)

[![coverage report](https://gitlab.com/_wugy/ratelimit/badges/master/coverage.svg)](https://gitlab.com/_wugy/ratelimit/commits/master)

Limit the rate of operations per time unit, use a [time.Ticker](https://golang.org/pkg/time/#NewTicker). This implementation refills the bucket based on the time elapsed between requests.

## Usage
Create new bucket
Refills every 18 seconds with 3 new requests and the bucket limit is 25

```golang
var apiBucketCart *ratelimit.Bucket
if apiBucket, err = ratelimit.NewBucket(time.Second*18, 3, 25); err != nil {
    log.Fatal("apiBucket.NewBucket")
}
```

to get a request simple

```golang
log.print("Wait for Slot")
<-apiBucketCart.Slot
```

